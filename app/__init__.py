from flask import Flask
from flask.ext.sqlalchemy import BaseQuery, SQLAlchemy

db = SQLAlchemy()


def load_models():
    from app.filings import models as fmodels
    from app.users import models as umodels


load_models()


def init_extensions(app):

    db.init_app(app)


def init_nocontext():
    app = Flask(__name__)
    app.config.from_object('config')
    app.jinja_options = app.jinja_options.copy()
    app.jinja_options['extensions'].extend(app.config['JINJA_EXTENSIONS'])

    init_views(app)
    engine = db.create_engine(app.config["SQLALCHEMY_DATABASE_URI"], convert_unicode=True)

    db.session = db.scoped_session(db.sessionmaker(autocommit=False, autoflush=False, bind=engine, query_cls=BaseQuery))
    # Uncomment the above two lines to make it so that we don't need application context

    db.metadata.bind = engine
    db.metadata.schema = 'secfilings'
    db.init_app(app)
    return app


def init_views(app):
    from app.users.views import mod as usersModule
    from app.filings.views import mod as filingsModule
    app.register_blueprint(usersModule)
    app.register_blueprint(filingsModule)


def create_app():
    app = Flask(__name__)
    app.config.from_object('config')
    app.jinja_options = app.jinja_options.copy()
    app.jinja_options['extensions'].extend(app.config['JINJA_EXTENSIONS'])

    init_extensions(app)
    init_views(app)
    return app

app = create_app()
