
$(document).ready(function() {
    ko.onDemandObservable = function (evaluator, owner) {
        var _value = ko.observable();

        var result = ko.computed({
            read: function() {

                if (!result.loaded()) {
                    evaluator.call(owner);
                }

                return _value();
            },

            write: function(newValue) {
                result.loaded(true);
                _value(newValue);
            },

            deferEvaluation: true
        });

        result.loaded = ko.observable();

        result.refresh = function() {
            result.loaded(false);
        };

        return result;
    };

    var FilingModel = function(data) {
        var self = this;
        self.filing_id = ko.observable(data.id);
        self.company_name = ko.observable(data.company_name);
        self.company_cik = ko.observable(data.company_cik);
        self.form_type = ko.observable(data.form_type);
        self.filing_date = ko.observable(data.filing_date);
        self.filing_url = ko.observable(data.filing_url);
        self.selected = ko.observable(false);
    };

    var CompanyModel = function(data) {
        var self = this;
        ko.utils.extend(self, new FilingModel(data));
        self.company_id = ko.observable(data.company_id);
    };

    var CompaniesModel = function(data) {
        var self = this;
        self.company_id = ko.observable(data.id);
        self.company_cik = ko.observable(data.company_cik);
        self.company_name = ko.observable(data.company_name);
        self.num_filings = ko.observable(data.num_filings);
        self.selected = ko.observable(false);
    };

    var FormsModel = function(data) {
        var self = this;
        self.form_id = ko.observable(data.id);
        self.form_type = ko.observable(data.form_type);
        self.num_forms = ko.observable(data.num_forms);
    };

    var FormModel = function(data) {
        var self = this;
        ko.utils.extend(self, new FilingModel(data));
        self.form_id = ko.observable(data.id);
    };

    var ListViewModel = function(model, modelstr, sortvar) {
        var self = this;
        self.model = ko.observable(modelstr);
        self.currentPage = ko.observable(1);
        self.totalPages = ko.observable();
        self.numPerPage = ko.observable(25);
        self.sortVar = ko.observable(sortvar);
        self.sortDir = ko.observable('desc');
        self.accModel = model;
        self.pageOptions = ko.observableArray([10, 25, 50, 100]);

        self.items = ko.onDemandObservable(self.getOthers(), self);

        self.update = function(newData) {
            self.currentPage(newData.page);
            self.totalPages(newData.num_pages);
            self.numPerPage(newData.per_page);
            self.sortVar(newData.sort_by);
            self.sortDir(newData.direction);
            var mappedData = ko.utils.arrayMap(newData.items, function(item) {
                return new self.accModel(item);
            });
            self.items(mappedData);
            console.log(mappedData);
        };
    };

    ListViewModel.prototype.getOthers = function() {
        var data = {
            caller: this.model(),
            paginate: this.numPerPage(),
            page: this.currentPage(),
            sort_by: this.sortVar(),
            order: this.sortDir(),
            form_id: this.model() === 'form' ? 1 : undefined,
            company_id: this.model() === 'company' ? 1 : undefined
        };
        console.log(data);
        $.ajax({
            type: 'GET',
            url: '/filings/_get_json',
            data: data,
            context: this,
            success: function(data) {
                this.update(data);
            },
            dataType: 'json'
        });
    };

    var filingview = new ListViewModel(FilingModel, 'filings', 'filing_date');
    var companiesview = new ListViewModel(CompaniesModel, 'companies', 'num_filings');
    var companyview = new ListViewModel(CompanyModel, 'company', 'filing_date');
    var formsview = new ListViewModel(FormsModel, 'forms', 'num_forms');
    var formview = new ListViewModel(FormModel, 'form', 'filing_date');
    ko.applyBindings(filingview);
});