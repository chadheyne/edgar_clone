from flask import render_template, flash, redirect, request, url_for, session, g, Blueprint, jsonify
from .models import Company, Filing, Altname, Forms
from .constants import VIEWS_MAP
from app.users.models import User
from app import db

mod = Blueprint('filings', __name__, url_prefix='/filings')


@mod.before_request
def before_request():
    """
    pull user's profile from the database before every request are treated
    """
    g.user = None
    if 'user_id' in session and session['user_id'] is not None:
        g.user = User.query.get(session['user_id'])
        g.user_role = g.user.role


@mod.route('/companies/', defaults={'page': 1}, methods=("GET", "POST"))
@mod.route('/companies/<int:page>/')
def show_companies(page):

    _returnargs = process_request(request, 'companies', page=page)
    companies = _returnargs['storage']

    if companies is None:
        flash('Company not found.')

    return render_template('filings/companies.html',
                           title='Home',
                           companies=companies,
                           )


@mod.route('/_get_json')
def get_json():

    page = request.args.get('page', 1, type=int)
    caller = request.args.get('caller', 'filings')
    company_id = request.args.get('company_id', 1, type=int)
    form_id = request.args.get('form_id', 1, type=int)

    _returnargs = process_request(request,
                                  caller,
                                  page=page,
                                  company_id=company_id,
                                  form_id=form_id)

    mapped_items = [item.serialize for item in _returnargs['storage'].items]
    num_pages = _returnargs['storage'].pages
    del _returnargs['storage']

    return jsonify(
                    items=mapped_items,
                    num_pages=num_pages,
                    **_returnargs
                    )


@mod.route('/companies/company/<int:company_id>/', defaults={'page': 1}, methods=("GET", "POST"))
@mod.route('/companies/company/<int:company_id>/<int:page>/')
def show_entries(company_id, page):

    _returnargs = process_request(request, 'company', company_id, page)
    filings = _returnargs['storage']
    company = Company.query.get(company_id)
    if not (company and filings):
        flash('No filings found for company {}'.format(company_id))

    return render_template('filings/company.html',
                           title='Home',
                           company=company,
                           filings=filings
                           )


@mod.route('/forms/', defaults={'page': 1}, methods=("GET", "POST"))
@mod.route('/forms/<int:page>/')
def show_types(page):
    _returnargs = process_request(request, 'forms', page=page)
    forms = _returnargs['storage']

    if not forms:
        flash('No forms found')

    return render_template('filings/forms.html',
                         forms=forms
                         )


@mod.route('/forms/type/<int:form_id>/', defaults={'page': 1}, methods=("GET", "POST"))
@mod.route('/forms/type/<int:form_id>/<int:page>/')
def type_entries(form_id, page):
    form = Forms.query.get(form_id)
    _returnargs = process_request(request, 'form', form_id=form.id, page=page)
    filings = _returnargs['storage']

    if not filings:
        flash('No forms found of type {}'.format(form.form_type))

    return render_template('filings/form.html',
                           form=form,
                           filings=filings,
                           )


def save_entries(request, caller='company'):
    action = request.form.get('action')
    user = g.user

    if caller in ('company', 'form'):
        for filing in request.form.getlist('filing_check', type=int):
            c = Filing.query.get(filing)
            if action == "save":
                user.filings.append(c)
            elif action == "delete" and c in user.filings:
                user.filings.remove(c)

    elif caller == 'companies':
        companies = request.form.getlist('company_check')
        for company in companies:
            for filing in company.filings:
                c = Filing.query.get(filing)
                if action == "save":
                    user.filings.append(c)
                elif action == "delete" and c in user.filings:
                    user.filings.remove(c)

    elif caller == 'forms':
        for forms in request.form.getlist('form_check'):
            for filing in forms.filings:
                c = Filing.query.get(filing)
                if action == "save":
                    user.filings.append(c)
                elif action == "delete" and c in user.filings:
                    user.filings.remove(c)

    db.session.add(user)
    db.session.commit()
    print(user.filings, action)


def process_request(request, caller='company', company_id=None, page=1, form_id=None):

    if request.form.get('action', '') in ('save', 'delete'):
        save_entries(request, caller)

    defaults = VIEWS_MAP[caller]

    num_per_page = request.args.get('paginate', defaults['paginate'], type=int)
    num_per_page = 100 if num_per_page > 100 else num_per_page
    ordering = request.args.get('order', defaults['order'])
    _sort = request.args.get('sort_by', defaults['sort_param'])

    if ordering.startswith('asc'):
        sort_param = defaults['storage'][0][_sort]
    else:
        sort_param = defaults['storage'][0][_sort].desc()

    if caller == 'company':
        company = Company.query.get(company_id)
        storage = company.filings.order_by(sort_param).paginate(page, num_per_page, False)
        storage = Filing.query.filter_by(company_id=company_id).order_by(sort_param).paginate(page, num_per_page, False)

    elif caller == 'companies':
        storage = Company.query.order_by(sort_param).paginate(page, num_per_page, False)

    elif caller == 'form':
        storage = Filing.query.filter_by(form_id=form_id).join(Company).order_by(sort_param).paginate(page, num_per_page, False)

    elif caller == 'forms':
        storage = Forms.query.order_by(sort_param).paginate(page, num_per_page, False)

    elif caller == 'filings':
        storage = Filing.query.order_by(sort_param).paginate(page, num_per_page, False)

    else:
        storage = None

    return {
            'storage': storage,
            'page': page,
            'per_page': num_per_page,
            'order_by': _sort,
            'direction': ordering
            }
