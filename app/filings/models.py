from app import db
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm.exc import NoResultFound


class Company(db.Model):
    __tablename__ = 'companies'
    __table_args__ = {'schema': 'secfilings'}

    id = db.Column(db.Integer, primary_key=True)
    company_cik = db.Column(db.Integer)
    company_name = db.Column(db.String(150))
    num_filings = db.Column(db.Integer)

    def __init__(self, company_cik, **kwargs):
        self.company_cik = company_cik
        self.add_filings(kwargs.get('temp_files', None))

    def __repr__(self):
        return '<Company CIK: {} - Company Name: {}>'.format(self.company_cik, self.company_name)

    @property
    def title_name(self):
        return self.company_name.title()

    @property
    def zeros_cik(self):
        return str(self.company_cik).zfill(10)

    @property
    def upper_case_name(self):
        return self.company_name.upper()

    @property
    def serialize(self):
        return dict(company_id=self.id,
                    company_cik=self.zeros_cik,
                    company_name=self.title_name,
                    num_filings=self.num_filings)

    @hybrid_property
    def get_company_name(self):
        if not hasattr(self, "alternate_names"):
            return "No company name has been associated with this CIK"
        return self.alternate_names.order_by(Altname.last_filing.desc()).first().company_name

    @hybrid_property
    def set_name(self):
        self.company_name = self.get_company_name

    @hybrid_property
    def get_num_filings(self):
        return sum((i.num_filings for i in self.alternate_names.all()))

    @hybrid_property
    def set_filings(self):
        self.num_filings = self.get_num_filings

    def add_filings(self, filings):
        import itertools
        for _name, _filings in itertools.groupby(filings, key=lambda f: f['Company Name']):
            _company = Altname.query.filter_by(company_name=_name, id=self.id).first()
            if _company is None:
                _company = Altname(_name)
                self.alternate_names.append(_company)
            _company.add_multiple(_filings)
            db.session.add(self)
        db.session.commit()


class Altname(db.Model):
    __tablename__ = 'altnames'
    __table_args__ = {'schema': 'secfilings'}
    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(150))
    first_filing = db.Column(db.Date)
    last_filing = db.Column(db.Date)
    num_filings = db.Column(db.Integer)

    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, schema='secfilings'))
    company = db.relationship("Company", backref=db.backref('alternate_names', lazy='dynamic'))

    def __init__(self, company_name, first_filing=None, last_filing=None, num_filings=0):
        self.company_name = company_name
        self.first_filing = first_filing
        self.last_filing = last_filing
        self.num_filings = num_filings

    def __repr__(self):
        return '<Company {}>'.format(self.company_name)

    @property
    def serialize(self):
        return dict(company_id=self.company.id,
                    altname_id=self.id,
                    company_name=self.company_name.title(),
                    first_filing=self.first_filing.isoformat(),
                    last_filing=self.last_filing.isoformat(),
                    num_filings=self.num_filings,
                    company_cik=self.company.zeros_cik,
                    updated_name=self.company.title_name)

    def add_filing(self, filing):
        if filing not in self.company.filings:
            self.num_filings += 1
            self.company.num_filings += 1
            self.company.filings.append(filing)

    def add_multiple(self, filings):
        for temp in filings:
            if not (self.first_filing <= temp['Date Filed'] <= self.last_filing):
                continue
            filing = Filing(temp['Form Type'], temp['Date Filed'], 'ftp://ftp.sec.gov/' + temp['Filename'])
            form = Forms.query.filter_by(form_type=filing.form_type).first()
            if form is not None:
                form.add_form()
            else:
                form = Forms(filing.form_type)
            filing.form_id = form.id
            self.add_filing(filing)

    @property
    def update_date(self, commit=True):
        self.first_filing = min(self.company.filings, key=lambda g: g.filing_date).filing_date
        self.last_filing = max(self.company.filings, key=lambda g: g.filing_date).filing_date
        if commit:
            db.session.add(self)
            db.session.commit()


class Forms(db.Model):
    __tablename__ = 'formtypes'
    __table_args__ = {'schema': 'secfilings'}

    id = db.Column(db.Integer, primary_key=True)

    form_type = db.Column(db.String(10))
    num_forms = db.Column(db.Integer)

    def __init__(self, form_type, num_forms=0):
        self.form_type = form_type
        self.num_forms = num_forms

    def __repr__(self):
        return '<Form type {!r}>'.format(self.form_type)

    @property
    def serialize(self):
        return dict(form_id=self.id,
                    form_type=self.form_type,
                    num_forms=self.num_forms)

    def add_form(self):
        self.num_forms += 1


class Filing(db.Model):
    __tablename__ = 'filings'
    __table_args__ = {'schema': 'secfilings'}

    id = db.Column(db.Integer, primary_key=True)
    form_type = db.Column(db.String(10))
    filing_date = db.Column(db.Date)
    filing_url = db.Column(db.String(70))
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, schema='secfilings'))
    company = db.relationship("Company", backref=db.backref('filings', lazy='dynamic'))

    form_id = db.Column(db.Integer, db.ForeignKey(Forms.id, schema='secfilings'))
    form = db.relationship("Forms", backref=db.backref('filings', lazy='dynamic'))

    def __init__(self, form_type, filing_date, filing_url):
        self.form_type = form_type
        self.filing_date = filing_date
        self.filing_url = filing_url

    def __repr__(self):
        return '<Filing {!r}>'.format(self.filing_url)

    @property
    def generator_stream(self):
        return ('"' + str(_var) + '"' for _var in (self.company.company_name, self.company.company_cik, self.filing_date,
                self.filing_url, self.form_type))

    @property
    def generator_csv(self):
        return {'Company Name': self.company.company_name,
                'CIK': self.company.company_cik,
                'Date Filed': self.filing_date,
                'Filename': self.filing_url,
                'Form Type': self.form_type}

    @property
    def serialize(self):
        return dict(filing_id=self.id,
                    company_id=self.company.id,
                    company_name=self.company.title_name,
                    company_cik=self.company.zeros_cik,
                    filing_date=self.filing_date.isoformat(),
                    filing_url=self.filing_url,
                    form_type=self.form_type)
