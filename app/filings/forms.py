from flask.ext.wtf.html5 import (
        SearchField, DateField, IntegerField,
        Form, SubmitField
        )


class SearchForm(Form):
    startdate = DateField("Start Date", format="%Y-%m-%d")
    enddate = DateField("End Date", format="%Y-%m-%d")
    company_cik = IntegerField("Company CIK")
    company_name = SearchField("Company Name")
    submit = SubmitField(label='Search')
