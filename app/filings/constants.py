from .models import Company, Filing, Forms
COMPANIES_PER_PAGE = FILINGS_PER_PAGE = 20

COMPANY_MAP = {
    'company_id': Company.id,
    'company_name': Company.company_name,
    'company_cik': Company.company_cik,
    'num_filings': Company.num_filings,
    'filings': "Company.filings"
}

FILING_MAP = {
    'filing_id': Filing.id,
    'form_type': Filing.form_type,
    'filing_date': Filing.filing_date,
    'filing_url': Filing.filing_url,
    'company_id': Filing.company,
    'company_name': Company.company_name
}

FORM_MAP = {
    'form_type': Forms.form_type,
    'num_forms': Forms.num_forms,
}

VIEWS_MAP = {

    'companies': {
        'paginate': COMPANIES_PER_PAGE,
        'order': '',
        'sort_param': 'num_filings',
        'storage': (COMPANY_MAP, ),
        'parent': Company,
    },

    'company': {
        'paginate': FILINGS_PER_PAGE,
        'order': '',
        'sort_param': 'filing_date',
        'storage': (FILING_MAP, ),
        'parent': Filing,
    },

    'forms': {
        'paginate': FILINGS_PER_PAGE,
        'order': '',
        'sort_param': 'num_forms',
        'storage': (FORM_MAP, ),
        'parent': Forms,
    },

    'form': {
        'paginate': FILINGS_PER_PAGE,
        'order': '',
        'sort_param': 'filing_date',
        'storage': (FILING_MAP, FORM_MAP),
        'parent': (Forms, Filing),
    },

    'filings': {
        'paginate': FILINGS_PER_PAGE,
        'order': '',
        'sort_param': 'filing_date',
        'storage': (FILING_MAP, ),
        'parent': Filing,
    }
}
