from flask import (Blueprint, request, render_template,
                   flash, g, session, redirect, url_for,
                   Response, send_file)
from werkzeug import check_password_hash, generate_password_hash
import itertools
import datetime

from app import db

from app.users.forms import RegisterForm, LoginForm, UserForm
from app.users.models import User
from app.users.decorators import requires_login, requires_admin

from app.filings.models import Company, Filing

mod = Blueprint('users', __name__)


@mod.route('/')
@mod.route('/index')
def index():
    limit = request.args.get('limit', 25)
    filings = Filing.query.order_by(Filing.filing_date.desc()).limit(limit)

    return render_template("index.html",
                           title='Home',
                           filings=filings,
                           limit=limit
                           )


@mod.route('/users/')
@requires_login
def home():
    user = g.user
    user_filings = dict((k, list(v)) for k, v in itertools.groupby(user.filings, key=lambda f: f.company.company_name))
    return render_template("users/profile.html", user=user, filings=user_filings)


@mod.before_request
def before_request():
    """
    pull user's profile from the database before every request are treated
    """
    g.user = None
    if 'user_id' in session and session['user_id'] is not None:
        g.user = User.query.get(session['user_id'])
        g.user_role = g.user.role


@mod.route('/users/login/', methods=['GET', 'POST'])
def login():
    """
    Login form
    """
    form = LoginForm(request.form)
    # make sure data are valid, but doesn't validate password is right
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
            # we use werzeug to validate user's password
        if user and check_password_hash(user.password, form.password.data):
            # the session can't be modified as it's signed,
            # it's a safe place to store the user id
            session['user_id'] = user.id
            session['user_role'] = user.role
            flash('Welcome %s' % user.name)
            return redirect(url_for('users.home'))
        flash('Wrong email or password', 'error-message')
    return render_template("users/login.html", form=form)


@mod.route('/users/logout/', methods=["GET", "POST"])
def logout():
    """
    Logout form
    """
    session.pop('user_id', None)
    session.pop('user_role', None)
    return redirect(url_for('filings.index'))


@mod.route('/users/register/', methods=['GET', 'POST'])
def register():
    """
    Registration Form
    """
    form = RegisterForm(request.form)
    if form.validate_on_submit():
      # create an user instance not yet stored in the database
        user = User(name=form.name.data, email=form.email.data,
            password=generate_password_hash(form.password.data))
      # Insert the record in our database and commit it
        db.session.add(user)
        db.session.commit()

      # Log the user in, as he now has an id
        session['user_id'] = user.id

      # flash will display a message to the user
        flash('Thanks for registering')
      # redirect user to the 'home' method of the user module.
        return redirect(url_for('users.home'))
    return render_template("users/register.html", form=form)


@mod.route('/admin/')
@requires_admin
def admin_index():
    return render_template('admin/index.html')


@mod.route('/admin/users/', methods=["GET"])
@requires_admin
def admin_users():
    users = User.query.all()
    return render_template('admin/users.html', users=users)


@mod.route('/admin/users/edit', defaults={'user': 1})
@mod.route('/admin/users/edit/<int:user>', methods=['GET', 'POST'])
@requires_admin
def edit_users(user):
    user = User.query.get(user)
    form = UserForm(request.form, obj=user)
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash('Successfully updated {} to role {}'.format(user.name, user.role))
    return render_template('admin/edit_users.html', form=form)


@mod.route('/admin/companies/', defaults={'page': 1})
@mod.route('/admin/companies/<int:page>')
@requires_admin
def admin_view(page):
    companies = Company.query.order_by(Company.num_filings.desc()).paginate(page, 20, False)

    return render_template('admin/companies.html',
                           title='Admin',
                           companies=companies
                           )


@mod.route('/users/file/', methods=("GET", "POST"))
@requires_login
def generate_filings():
    if request.form.get('action', '') in ('download', 'delete'):
        return do_filings(request)
    return redirect(url_for('users.home'))


def do_filings(request):
    import tempfile
    import csv
    action = request.form.get('action')
    user = g.user
    filings = [Filing.query.get(i) for i in request.form.getlist('filing_check', type=int)] or user.filings

    if action == "download":
        attachment_filename = request.form.get('filename', 'edgar_filings_{}.csv'.format(datetime.date.today().strftime('%m_%d_%y')))
        tf = tempfile.NamedTemporaryFile(mode="w+", newline='', delete=False)
        temp_csv = csv.writer(tf, csv.QUOTE_ALL)
        temp_csv.writerow(('Company Name', 'Company CIK', 'Filing Date', 'Filing URL', 'Form Type'))
        for filing in filings:
            print(filing.generator_csv)
            temp_csv.writerow(filing.generator_csv)
        tf.close()
        tf2 = open(tf.name, 'rb')
        return send_file(tf2, as_attachment=True,
                attachment_filename=attachment_filename)

    elif action == 'delete':
        for filing in filings:
            c = Filing.query.get(filing)
            if c in user.filings:
                user.filings.remove(c)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('users.home'))
