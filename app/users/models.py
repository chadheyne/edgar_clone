from app import db
from app.filings.models import Filing
from app.users import constants as USER


class User(db.Model):
    __tablename__ = "users"
    __table_args__ = {'schema': 'secfilings'}
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String)
    role = db.Column(db.SmallInteger, default=USER.USER)
    status = db.Column(db.SmallInteger, default=USER.NEW)
    filings = db.relationship(Filing, secondary='secfilings.user_filings',
                backref=db.backref('users', lazy='dynamic'))
    #filings = association_proxy('filings_db.relationship', 'id',
    #            creator=lambda filing: Filing())

    def __init__(self, name=None, email=None, password=None):
        self.name = name
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User {}>'.format(self.name)

    @property
    def get_status(self):
        return USER.STATUS[self.status]

    @property
    def get_role(self):
        return USER.ROLE[self.role]

    @property
    def serialize(self):
        result = dict(name=self.name,
                      email=self.email)

        return result if self.role != 0 else result.update(role=self.role)

user_filings = db.Table('user_filings', db.metadata,
               db.Column('user_id', db.Integer, db.ForeignKey(User.id, schema='secfilings')),
               db.Column('filing_id', db.Integer, db.ForeignKey(Filing.id, schema='secfilings')),
               schema='secfilings'
               )
