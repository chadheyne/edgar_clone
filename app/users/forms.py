from wtforms import TextField, PasswordField, BooleanField, SelectField
from flask.ext.wtf import Form
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms.ext.sqlalchemy.orm import model_form
from app.users.models import User


class LoginForm(Form):
    email = TextField('Email address', [DataRequired(), Email()])
    password = PasswordField('Password', [DataRequired()])


class RegisterForm(Form):
    name = TextField('NickName', [DataRequired()])
    email = TextField('Email address', [DataRequired(), Email()])
    password = PasswordField('Password', [DataRequired()])
    confirm = PasswordField('Repeat Password', [
        DataRequired(),
        EqualTo('password', message='Passwords must match')
        ])
    accept_tos = BooleanField('I accept the TOS', [DataRequired()])


class UserForm(Form):
    name = TextField('Nickname')
    email = TextField('Email address', [Email()])
    role = SelectField('Role', choices=[('0', 'Admin'), ('1', 'Staff'), ('2', 'User')])
