#!/usr/bin/env python
from flask.ext.script import Shell, Manager
from app import create_app, db
from app.filings import models as filings_models
from app.users import models as user_models

app = create_app()


def _make_context():
    return(dict(edgar=app, db=db, users=user_models, filings=filings_models))


manager = Manager(app)
manager.add_command("shell", Shell(make_context=_make_context))

if __name__ == "__main__":
    manager.run()
